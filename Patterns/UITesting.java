import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class UITesting extends BaseTest {
    // Для решения проблем с драйвером:
    // https://stackoverflow.com/questions/60296873/sessionnotcreatedexception-message-session-not-created-this-version-of-chrome/62127806

    @After
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void pageTest() {
        TestingService service = PageFactory.initElements(driver, TestingService.class);
        service.open();

        assertTrue(service.isShowAnswerButtonExist());
        assertTrue(service.isLinkForAnswerExist());
        assertTrue(service.isHideAnswerButtonExist());
    }


}

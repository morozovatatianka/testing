import org.assertj.core.api.AbstractAssert;
import org.json.JSONObject;

public class JsonCustomMatcher extends AbstractAssert<JsonCustomMatcher, String> {
    protected JsonCustomMatcher(String s) {
        super(s, JsonCustomMatcher.class);
    }

    public JsonCustomMatcher containsSuccessAndStatus() {
        JSONObject obj = new JSONObject(this.actual);
        if(!obj.getString("status").equals("success")) {
            failWithMessage("given string not contain status and success");
        }
        return this;
    }
}

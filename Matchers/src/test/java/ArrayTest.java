import org.junit.jupiter.api.Test;

public class ArrayTest {
    @Test
    void arrayTest() {
        int[] array1 = new int[] { 5, 2, 4, 7, 12, 0 };
        int[] array2 = new int[] { 2, 7, 5, 12, 4, 0 };
        ArrayCustomMatcherTest.assertThat(array1).containingInAnyOrder(array2);
    }
}

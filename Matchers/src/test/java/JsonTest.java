import org.junit.jupiter.api.Test;

public class JsonTest {
    @Test
    void jsonTest() {
        String json1 = "{\"status\": \"success\"}";
        JsonCustomMatcherTest.assertThat(json1).containsSuccessAndStatus();
    }
}

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.api.AbstractIntArrayAssert;

import java.util.HashSet;
import java.util.Set;

public class ArrayCustomMatcher<SELF extends AbstractIntArrayAssert<SELF>> extends AbstractAssert<ArrayCustomMatcher<SELF>, int[]> {

    protected ArrayCustomMatcher(int[] ints) {
        super(ints, ArrayCustomMatcher.class);
    }

    public ArrayCustomMatcher containingInAnyOrder(int... values) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();
        for(int value: this.actual) {
            set1.add(value);
        }
        for(int value: values) {
            set2.add(value);
        }
        if(!set1.equals(set2)) {
            failWithMessage("Arrays not containing in any order");
        }
        return this;
    }
}